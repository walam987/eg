<?php

/**
 * Created by PhpStorm.
 * User: michal
 * Date: 27.05.16
 * Time: 15:25
 */

//declare(strict_types=1);
//use jaka zmiana
//use jakaś kolejna zmiana
//use jakaś kolejna zmiana2


//use jakis przykladowy tekst do cwiczenia GIT

//use jeszcze jakaś zmiana
class Kalkulator
{

    private $resultLastConstructorInvoking = "";

        /**
     * Kalkulator constructor. Calculates operations included in $argv array or optional in $operationsInLine string.
     * Don't forget to call showResult() method then.
     * @param $argv operations - to calculate included as arguments invoked in the command line
     * @param string|null $operationsInLine - operations to calculate included in one string
     */
    public function __construct($argv = null, string $operationsInLine = null)
    {
        if (isset($argv)) {
            $this->resultLastConstructorInvoking = $this->calculateUsingArgumentsTheCommandLine($argv);
        }
        else if (isset($operationsInLine))
        {
            $this->resultLastConstructorInvoking = $this->calculateUsingString($operationsInLine);
        }
        else
        {
            $this->resultLastConstructorInvoking = 0;
        }
    }

    /**show result of last invoking Kalkulator constructor with arguments to calculation
     * @return string
     */
    public function showResultLastConstructorInvoking() : string
    {
        return $this-> resultLastConstructorInvoking;
    }

    /**calculate operations included in $operationsInLine (string)
     * @param string $operationsInLine - operations to calculate included in one string
     * @return string
     */
    public static function calculateUsingString(string $operationsInLine) : string
    {
        $operationsAfterRemovalOfBrackets = null;
        $expressions = null;
        $inTotal = null;
        $result = null;

        try {

            $operationsInLine = self:: deleteSpaces($operationsInLine);

            self:: validate($operationsInLine);

            $operationsInLine = self:: changeCommaToDot($operationsInLine);

            $operationsAfterRemovalOfBrackets = self:: simplyfyOperationsByRemovingOfBrackets($operationsInLine);

            $expressions = self:: divideIntoExpressions($operationsAfterRemovalOfBrackets);

            $inTotal = self:: calculateExpressions($expressions);

            $result = self:: view($operationsInLine) . " = " . $inTotal . "\n";

        }catch(Exception $e){

            $result .= "Operations has been badly typed, try again\n" . "(" . $e -> getMessage() . ")\n";
        }

        return $result;
    }


    /**calculate operations included in $argv (array of arguments passed to script)
     * @param array $argv operations - to calculate included as arguments invoked in the command line
     * @return string - result of calculation
     */
    public static function calculateUsingArgumentsTheCommandLine(array $argv) : string
    {
        $operationsAfterRemovalOfBrackets = null;
        $expressions = null;
        $inTotal = null;
        $result = null;

        try {

            $operationsInLine = self:: bringIntoOneLine($argv);

            $operationsInLine = self:: deleteSpaces($operationsInLine);

            self:: validate($operationsInLine);

            $operationsInLine = self:: changeCommaToDot($operationsInLine);

            $operationsAfterRemovalOfBrackets = self:: simplyfyOperationsByRemovingOfBrackets($operationsInLine);

            $expressions = self:: divideIntoExpressions($operationsAfterRemovalOfBrackets);

            $inTotal = self:: calculateExpressions($expressions);

            $result = self:: view($operationsInLine) . " = " . $inTotal . "\n";

        }catch(Exception $e){

            $result .= "Operations has been badly typed, try again\n" . "(" . $e -> getMessage() . ")\n";
        }

        return $result;
    }

    /**check if the string containg of operations is correct
     * @param string $operationsInLine - operations to calculate included in one string
     * @return bool
     * @throws Exception
     */
    private function validate(string $operationsInLine) : bool
    {
        $result = true;
        for ($i = 0; $i < strlen($operationsInLine); $i++){
            $sign = $operationsInLine[$i];
            if ( !(self:: isOperator($sign) || is_numeric($sign) || $sign == '(' || $sign == ')' || $sign == ',' || $sign == '.' ) ) {
                $result = false;
            }
        }
        if (!$result)
        {
            $message = "enter numbers and , . (if you aren't typing in the integers), operators * / + - and brackets ( )";
            $message .= "\nif you want to use operations with brackets, type all operations in apostophes or in quoation marks";
            throw new Exception ($message);
        }
        return $result;
    }


    /**changes comma if exists to dot
     * @param string $operationsInLine - operations to calculate included in one string
     * @return string
     */
    private function changeCommaToDot(string $operationsInLine) : string
    {
        for ($i = 0; $i < strlen($operationsInLine); $i++)
        {
            if ($operationsInLine[$i] == ',')
            {
                $operationsInLine[$i] = ".";
            }
        }
        return $operationsInLine;
    }

    private function deleteSpaces(string $text) : string
    {
        $textWithoutSpaces = null;
        for ($i = 0; $i < strlen($text); $i++)
        {
            if ($text[$i] != ' ')
            {
                $textWithoutSpaces .= $text[$i] ;
            }
        }
        return $textWithoutSpaces;
    }


    /** Simplyfy operations by removing the brackets in every level (by performing the calculations in them)
     * @param string $operationsInLine - operations to calculate included in one string
     * @return string
     * @throws Exception
     */
    private function simplyfyOperationsByRemovingOfBrackets(string $operationsInLine) : string
    {
        $howDeepTheLevel = self:: checkHowDeepTheLevelOfBrackets($operationsInLine);

        $simplyfiedOperations = $operationsInLine;
        for ($i = $howDeepTheLevel; $i >= 0 ; $i--){
            $simplyfiedOperations = self:: simplyfyOperations($simplyfiedOperations, $i);
        }

        return $simplyfiedOperations;
    }

    /** (expression - the operation consisting of the multiplication and division)
     * @param $expressionInLine - operations to calculate included in one string
     * @return array of strings containing expressions
     */
    private function divideIntoExpressions(string $expressionInLine) : array
    {
        $expressions = null;

        for ($i = 0, $a = 0; $i < strlen($expressionInLine); $i++) {
            if ($i + 1 < strlen($expressionInLine)) {
                if ($expressionInLine[$i + 1] == "+" || $expressionInLine[$i + 1] == "-") {
                    if ($expressionInLine[$i] != '/' && $expressionInLine[$i] != '*') {
                        //(isset($expressions[$a]) == false) ? ($expressions[$a] = $expressionInLine[$i]) : ($expressions[$a] .= $expressionInLine[$i]);
                        if (!isset($expressions[$a])) {
                            $expressions[$a] = $expressionInLine[$i];
                        } else {
                            $expressions[$a] .= $expressionInLine[$i];
                        }
                        $a++;
                    } else {
                        //tutaj dodaj element do wyrazenia
                        if (!isset($expressions[$a])) {
                            $expressions[$a] = $expressionInLine[$i];
                        } else {
                            $expressions[$a] .= $expressionInLine[$i];
                        }
                    }
                } //dodaj kolejny element wyrazenia
                else {
                    //tutaj dodaj element do wyrazenia
                    if (!isset($expressions[$a])) {
                        $expressions[$a] = $expressionInLine[$i];
                    } else {
                        $expressions[$a] .= $expressionInLine[$i];
                    }
                }
            }
            else{
                //tutaj konczy sie expression (koniec elementow)
                if (!isset($expressions[$a])) {
                    $expressions[$a] = $expressionInLine[$i];
                }else{
                    $expressions[$a] .= $expressionInLine[$i];
                }
                $a++;
            }
        }

        return $expressions;
    }

    /** Simplyfy operations by removing the brackets in $whatLevel level (by performing the calculations in them)
     * @param string $operationsInLine - operations to calculate included in one string
     * @param int $whatLevel - depth level operations in parentheses (brackets)
     * @return string
     */
    private function simplyfyOperations(string $operationsInLine, int $whatLevel) : string
    {
        $operations = null;
        $leftBracket = 0;
        $operationInBrackets = null;
        $inBracketsArea = false;
        //init an empty array (faster than  init. like var foo = new Array)
        $signsOperationsInBrackets = [];  //array(0=> "_0_", 1=> "_1_", 2=> "_2_");

        if ($whatLevel == 0)
        {
            return $operationsInLine;
        }

        for ($i = 0, $a = -1; $i < strlen($operationsInLine); $i++) {
            if ($operationsInLine[$i] == "(") {
                $leftBracket++;
                if ($whatLevel == $leftBracket)
                {
                    $inBracketsArea = true;
                    $a++;
                    $signsOperationsInBrackets = self:: addSignToArray($signsOperationsInBrackets);
                    $operations .= $signsOperationsInBrackets[$a];
                }
                else{
                    $operations .= $operationsInLine[$i];
                }
            }
            else if ($operationsInLine[$i] == ")"){
                if ($whatLevel == $leftBracket){
                    $inBracketsArea = false;
                }
                else{
                    $operations .= $operationsInLine[$i];
                }
                $leftBracket--;
            }
            else if ($whatLevel == $leftBracket ){
                if (isset($operationInBrackets[$a])){
                    $operationInBrackets[$a] .= $operationsInLine[$i];
                }else{
                    $operationInBrackets[$a] = $operationsInLine[$i];
                }
            }
            else {
                $operations .= $operationsInLine[$i];
            }
        }

        $calculatedOperationsInBrackets = self:: calculateOperationInEveryPositionOfArray($operationInBrackets);
        $operations = self:: putCalculatedOperationInBracketsIntoOperationsInTheSignsPlace($operations, $calculatedOperationsInBrackets, $signsOperationsInBrackets);

        return $operations;
    }


    /** *put calculated operation in brackets into operations line in the signs place
     * @param string $operations
     * @param array $calculatedOperationInBrackets
     * @param array $signs
     * @return string
     */
    private function putCalculatedOperationInBracketsIntoOperationsInTheSignsPlace(string $operations, array $calculatedOperationInBrackets, array $signs) : string
    {
        for ($i = 0; $i < count($signs); $i++)
        {
            //fits into $operations result of operation in $operationInBrackets
            $indexOfOpInBrackets = strpos($operations, $signs[$i]);
            $operations = substr_replace($operations, $calculatedOperationInBrackets[$i], $indexOfOpInBrackets, strlen($signs[$i]));
        }

        return $operations;
    }

    /** Add sign to array as '_' with next number for this will result in the following
     * like 0=> "_0_", 1=> "_1_", 2=> "_2_" etc...
     * @param array $signsOperationsInBrackets
     * @return array
     */
    private function addSignToArray(array $signsOperationsInBrackets) : array
    {
        $signsOperationsInBrackets[count($signsOperationsInBrackets)] = "_" . count($signsOperationsInBrackets) . "_";
        return $signsOperationsInBrackets;
    }


    /**
     * @param string $operationsInLine
     * @return int - max depth level operations in parentheses (brackets)
     * @throws Exception
     */
    private function checkHowDeepTheLevelOfBrackets(string $operationsInLine) : int
    {
        $deepLevel = 0;
        $leftBracket = 0;
        for ($i = 0, $a = 0; $i < strlen($operationsInLine); $i++) {
            if ($operationsInLine[$i] == "(") {
                $leftBracket++;
                $deepLevel = $leftBracket;
            } else if ($operationsInLine[$i] == ")") {
                if ($leftBracket > 0) {
                    $leftBracket--;
                } else {
                    $message = "the number of brackets does not match";
                    throw new Exception($message);
                }
            }
        }
        return $deepLevel;
    }


    /**
     * @param array $argv - operations to calculate included as arguments invoked in the command line
     * @return string - operations to calculate included in one string
     */
    private function bringIntoOneLine(array $argv) : string
    {
        $operationsInLine = null;
        for ($i = 1; $i < count($argv); $i++) {
            $operationsInLine .= $argv[$i];
        }
        return $operationsInLine;
    }

    private function calculateMultiplicationAndDivision(string $expression) : string
    {
        $numbers= self:: pullNumbers($expression);
        $operators = self:: pullOperatorsOfMultiplicationAndDivision($expression);

        if (count($numbers) == 0){
            throw new Exception("there are not entered the number in the one or more operation");
        }
        else if (strlen($operators) >= count($numbers)) {
            throw new Exception("operator typed more than numbers");
        }
        else {
            //calculate
            $expression = $numbers[0];
            for ($i = 0; $i < strlen($operators); $i++) {
                if ($operators[$i] == "*") {
                    $expression *= $numbers[$i + 1];
                } else if ($operators[$i] == "/") {
                    if ($numbers[$i + 1] != 0) {
                        $expression /= $numbers[$i + 1];
                    } else {
                        throw new Exception("division by zero");
                    }
                }
            }
        }
        return $expression;
    }

    private function calculateOperationInEveryPositionOfArray(array $operationInBrackets) : array
    {
        for ($i = 0; $i < count($operationInBrackets); $i++)
        {
            $operationInBrackets[$i] = self:: calculateOperation($operationInBrackets[$i]);
        }

        return $operationInBrackets;
    }

    /** calculate expressions and return in the form of a sum in the string
     * @param $expressions
     * @return result in the string
     */
    private function calculateExpressions(array $expressions) : string
    {
        $inTotal = 0;
        for ($i = 0; $i < count($expressions); $i++) {
            $inTotal += self:: calculateOperation($expressions[$i]);
        }
        return $inTotal;
    }

    /** calculate expression and return in the form of a sum in the string
     * @param $expression
     * @return result in the string
     * @throws Exception
     */
    private function calculateOperation(string $expression) : string
    {
        $expressions = self:: divideIntoExpressions($expression);

        $inTotal = 0;
        for ($i = 0; $i < count($expressions); $i++) {
            $inTotal += self:: calculateMultiplicationAndDivision($expressions[$i]);
        }

        return $inTotal;
    }

    /** pull numbers with their '-' sign of every one if exists
     * @param string $expression
     * @return result in the array of the string
     */
    private function pullNumbers(string $expression) : array
    {
        $numbers = null;

        $a = -1;
        $sign = null;
        $digit = false;
        for ($i = 0; $i < strlen($expression); $i++) {
            if ($expression[$i] == "+" || $expression[$i] == "-"){
                $sign = $expression[$i];
                $digit = false;
            }
            else if (is_numeric($expression[$i]) || $expression[$i] == '.' || $expression[$i] == ',') {
                if ($sign != false){
                    $numbers [++$a] = $sign .= $expression[$i];
                }
                else
                {
                    if ($digit == false)
                    {
                        $a++;
                        if (isset($numbers[$a]))
                            $numbers [$a] .= $expression[$i];
                        else
                            $numbers [$a] = $expression[$i];
                    }
                    else
                    {
                        $numbers [$a] .= $expression[$i];
                    }
                }

                $sign = false;
                $digit = true;
            }
            else{
                $digit = false;
            }
        }

        return $numbers;
    }

    private function pullOperatorsOfMultiplicationAndDivision(string $expression) : string
    {
        $operators = "";

        for ($i = 0; $i < strlen($expression); $i++) {
            if ($expression[$i] == "*" || $expression[$i] == "/") {
                $operators .= $expression[$i];
            }
        }

        return $operators;
    }


    /** view the operation has been typed in the command line
     * @param string $operationsInLine
     * @return string
     */
    private function view(string $operationsInLine) : string
    {
        //$view = $operationsInLine[0];
        $view = null;
        $bracket = false;
        $digit = false;
        for ($i = 0; $i < strlen($operationsInLine); $i++){
            if ($operationsInLine[$i] == '('){
                if ($bracket)
                {
                    $view .= $operationsInLine[$i];
                }
                else
                {
                    //pierwszy sign zawsze bez spacji
                    if ($i != 0) {
                        $view .= " " . $operationsInLine[$i];
                    }
                    else
                    {
                        $view .= $operationsInLine[$i];
                    }
                }
                $bracket = true;
                $digit = false;
            }
            else if ($operationsInLine[$i] == ')'){
                //sprawdz czy nastepne expression to nawias
                if ($i + 1 < strlen($operationsInLine) && $operationsInLine[$i + 1] == ')'){
                    $view .= $operationsInLine[$i];
                }
                else {
                    if ($i + 1 < strlen($operationsInLine)) {
                        $view .= $operationsInLine[$i] . " ";
                    }
                    else
                    {
                        $view .= $operationsInLine[$i];
                    }
                }
                $bracket = true;
                $digit = false;
            }
            //cyfra albo operatory
            else {
                if ($bracket){
                    $view .= $operationsInLine[$i];
                }
                else{
                    if ($digit && !self:: isOperator($operationsInLine[$i]))
                    {
                        //jesli poprzedni sign nie byl operatorem
                        if ($i > 0 && !self:: isOperator($operationsInLine[$i - 1]))
                        {
                            $view .= $operationsInLine[$i];
                        }
                        else {
                            //pierwszy sign zawsze bez spacji
                            if ($i != 0) {
                                $view .= " " . $operationsInLine[$i];
                            }
                            else
                            {
                                $view .= $operationsInLine[$i];
                            }
                        }
                    }
                    else {
                        //pierwszy sign zawsze bez spacji
                        if ($i != 0) {
                            $view .= " " . $operationsInLine[$i];
                        }
                        else
                        {
                            $view .= $operationsInLine[$i];
                        }
                    }
                }
                $bracket = false;
                $digit = true;
            }
        }

        return $view;
    }

    private function isOperator(string $sign) : bool
    {
        if ($sign == '+' || $sign == '-' || $sign == '/' || $sign == '*')
        {
            return true;
        }

        return false;
    }

}

?>
