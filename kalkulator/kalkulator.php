<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 16.05.16
 * Time: 11:22
 */

//use jakas zmiana
//use jakaś kolejna zmiana
//use jakaś kolejna zmiana2

//use jeszcze jakaś nowa zmiana
//////////////
require_once ('Kalkulator.php');

//statycznie
echo Kalkulator::calculateUsingArgumentsTheCommandLine($argv);
echo Kalkulator::calculateUsingString("2+2*2/(3-2*(2+3))");

//w inny sposob 1
$kalkulator = new Kalkulator();
echo $kalkulator->calculateUsingArgumentsTheCommandLine($argv);
echo $kalkulator->calculateUsingString("2+2*2/(3-2)");

//w inny sposob 2
$kalkulator = new Kalkulator($argv);
echo $kalkulator->showResultLastConstructorInvoking();


?>